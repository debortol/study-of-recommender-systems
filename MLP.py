import numpy as np
import math
from ast import literal_eval as make_tuple
from time import time
from keras.layers import Input, Dense, Embedding, Flatten, Concatenate
from keras.models import Model
from keras.optimizers import Adam
from keras.losses import BinaryCrossentropy
import matplotlib.pyplot as plt
import tensorflow as tf

start = time()

train_file_path = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/neural_collaborative_filtering/Data/ml-1m.train.rating"

def number_users_and_items(path):
    file = open(file=path, mode='r')
    num_users, num_items = 0, 0
    for line in file:
        split_line = line.split('\t')[:-1]
        userID, itemID = int(split_line[0]), int(split_line[1])
        if userID > num_users:
            num_users = userID
        if itemID > num_items:
            num_items = itemID
    file.close()
    return num_users, num_items

def get_train_set(path, num_negative_instances):
    num_items = number_users_and_items(path)[1]
    data = []
    file = open(file=path, mode='r')
    current_user = 0
    interactions_per_user = []
    for line in file:
        split_line = line.split('\t')[:-1]
        user, item = int(split_line[0]), int(split_line[1])
        if user == current_user:
            interactions_per_user.append(item)
            data.append([user, item, 1])
        if user != current_user:
            num_interactions = len(interactions_per_user)
            for i in range(num_negative_instances * num_interactions):
                random_item = np.random.randint(num_items)
                while (random_item in interactions_per_user):
                    random_item = np.random.randint(num_items)
                data.append([current_user, random_item, 0])
            data.append([user, item, 1])
            current_user = user
            interactions_per_user = [item]
    num_interactions = len(interactions_per_user)
    for i in range(num_negative_instances * num_interactions):
        random_item = np.random.randint(num_items)
        while (random_item in interactions_per_user):
            random_item = np.random.randint(num_items)
        data.append([current_user, random_item, 0])
    return np.array(data)


num_users, num_items = number_users_and_items(path=train_file_path)

print("\n*** Preprocessing of data ***")
train_set = get_train_set(path=train_file_path, num_negative_instances=4)
print("Number of users : {}".format(num_users))
print("Number of items : {}".format(num_items))
print("Number of training interactions : {}".format(len(train_set)))
train_features = [train_set[:,0], train_set[:,1]]
train_labels = train_set[:,2]
print("Done !\n")

d = 32
h = int(d/2)
batch_size = 256
"""
## MLP model
user_input_MLP = Input(shape=(1,), dtype='int32', name='user_input_MLP')
item_input_MLP = Input(shape=(1,), dtype='int32', name='item_input_MLP')

user_embedding_MLP = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_MLP')
item_embedding_MLP = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_MLP')

user_latent_MLP = Flatten()(user_embedding_MLP(user_input_MLP))
item_latent_MLP = Flatten()(item_embedding_MLP(item_input_MLP))

concatenation = Concatenate()([user_latent_MLP, item_latent_MLP])

layer1 = Dense(units=64, activation='relu', name='layer1')(concatenation)
layer2 = Dense(units=32, activation='relu', name='layer2')(layer1)
layer3 = Dense(units=16, activation='relu', name='layer3') (layer2)
layer4 = Dense(units=8, activation='relu', name='layer4') (layer3)
prediction_MLP = Dense(units=1, activation='sigmoid', name='prediction_MLP')(layer4)

MLP = Model(inputs=[user_input_MLP, item_input_MLP], outputs=prediction_MLP)
MLP.compile(optimizer=Adam(), loss=BinaryCrossentropy())
MLP.name = "MLP"
"""
def get_model_MLP(num_users, num_items):
    user_input_MLP = Input(shape=(1,), dtype='int32', name='user_input_MLP')
    item_input_MLP = Input(shape=(1,), dtype='int32', name='item_input_MLP')

    user_embedding_MLP = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_MLP')(user_input_MLP)
    item_embedding_MLP = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_MLP')(item_input_MLP)

    user_latent_MLP = Flatten()(user_embedding_MLP)
    item_latent_MLP = Flatten()(item_embedding_MLP)

    concatenation = Concatenate()([user_latent_MLP, item_latent_MLP])

    layer1 = Dense(units=64, activation='relu', name='layer1')(concatenation)
    layer2 = Dense(units=32, activation='relu', name='layer2')(layer1)
    layer3 = Dense(units=16, activation='relu', name='layer3') (layer2)
    layer4 = Dense(units=8, activation='relu', name='layer4') (layer3)
    prediction_MLP = Dense(units=1, activation='sigmoid', name='prediction_MLP')(layer4)

    MLP = Model(inputs=[user_input_MLP, item_input_MLP], outputs=prediction_MLP)
    MLP.compile(optimizer=tf.train.AdamOptimizer(), loss=BinaryCrossentropy())
    return MLP

MLP = get_model_MLP(num_users=num_users, num_items=num_items)

def getHitRatio(ranklist, K, positive_item):
    if positive_item in ranklist[:K]:
        return 1
    else:
        return 0

def getNDCG(ranklist, K, positive_item):
    if positive_item in ranklist[:K]:
        ranking_of_positive_item = np.where(ranklist == positive_item)[0][0]
        return math.log(2)/math.log(2+ranking_of_positive_item)
    else:
        return 0

def getNDCG2(ranklist, K, positive_item):
    if positive_item in ranklist[:K]:
        ranking_of_positive_item = np.where(ranklist == positive_item)[0][0]
        return ranking_of_positive_item/math.log(2+ranking_of_positive_item) #log(base2)(ranking+1)
    else:
        return 0

def rank(item_scores):
    list_item_scores = item_scores.tolist()
    ranklist = sorted(list_item_scores, key=lambda item_score: item_score[1], reverse=True)
    ranklist = np.array(ranklist)[:,0].astype('int64')
    return ranklist

def evaluate_model(model, negative_file_path, K):
    hits, ndcgs = [],[]
    negative_file = open(file=negative_file_path, mode="r")
    for line in negative_file:
        split_line = line.split(sep="\t")
        (user, positive_item) = make_tuple(split_line[0])
        items = np.array([positive_item]+[int(split_line[i]) for i in range(1,len(split_line))])
        users = np.full(shape=len(items), fill_value=user)
        predictions = model.predict(x=[users,items], batch_size=100, verbose=0)
        map_item_scores = np.concatenate((items.reshape((100,1)), predictions), axis=1)
        ranklist_items = rank(item_scores=map_item_scores)
        hr = getHitRatio(ranklist=ranklist_items, K=K, positive_item=positive_item)
        ndcg = getNDCG(ranklist=ranklist_items, K=K, positive_item=positive_item)
        hits.append(hr)
        ndcgs.append(ndcg)
    hits = np.array(hits)
    ndcgs = np.array(ndcgs)
    return hits.mean(), ndcgs.mean()

def print_model_performance(model, negative_file_path, K):
    print("\n*** Evaluation of the model ***")
    hits_mean, ndcgs_mean = evaluate_model(model=model, negative_file_path=negative_file_path, K=K)
    print("Hit Rate : {:.2f}\nNDCG : {:.2f}\n".format(hits_mean, ndcgs_mean))

negative_file_path = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/neural_collaborative_filtering/Data/ml-1m.test.negative"

print_model_performance(model=MLP, negative_file_path=negative_file_path, K=10)

MLP.summary()

print("\n*** Training of the model ***\n")

K = 10
epochs = 2
first_hit, first_ndcg = evaluate_model(model=MLP, negative_file_path=negative_file_path, K=K)
first_loss = MLP.evaluate(x=train_features, y=train_labels, batch_size=batch_size, verbose=0)
hits, ndcgs, loss = [first_hit], [first_ndcg], [first_loss]
for e in range(epochs):
    history = MLP.fit(x=train_features, y=train_labels, batch_size=batch_size, epochs=1, verbose=2, shuffle=True)
    hit, ndcg = evaluate_model(model=MLP, negative_file_path=negative_file_path, K=K)
    hits.append(hit)
    ndcgs.append(ndcg)
    loss.append(history.history["loss"][0])

print_model_performance(model=MLP, negative_file_path=negative_file_path, K=10)

run_time = time()-start
print("Running time : {:.0f} min {:.0f} sec".format(run_time//60, run_time%60))

iterations = [e for e in range(epochs+1)]
path_to_save_figures = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/Courbes/"
color_MLP = "rebeccapurple"

plt.figure("Loss")
plt.plot(iterations, loss, color=color_MLP)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("Training loss")
plt.legend(("MLP",))
plt.title("Loss")
plt.savefig(fname=path_to_save_figures+"loss.png")

plt.figure("Hit Ratio")
plt.plot(iterations, hits, color=color_MLP)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("Hit Ratio")
plt.legend(("MLP",))
plt.title("Hit Ratio")
plt.savefig(fname=path_to_save_figures+"hr.png")

plt.figure("NDCG")
plt.plot(iterations, ndcgs, color=color_MLP)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("NDCG")
plt.legend(("MLP",))
plt.title("NDCG")
plt.savefig(fname=path_to_save_figures+"ndcg.png")

plt.show()
