import pandas as pd
import numpy as np

travel_filepath = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/Travel_28K.csv"

travel_file = open(file=travel_filepath, mode="r")

df = pd.read_csv(filepath_or_buffer=travel_filepath)

ones = np.ones(shape=(len(df.values),1))
data = np.concatenate((df.values, ones), axis=1).astype(dtype="int64")
