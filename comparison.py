import numpy as np
import math
from ast import literal_eval as make_tuple
from time import time
from keras.layers import Input, Dense, Embedding, Flatten, Concatenate, Multiply
from keras.models import Model
from keras.optimizers import Adam
from keras.losses import BinaryCrossentropy
import matplotlib.pyplot as plt

start = time()

train_file_path = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/neural_collaborative_filtering/Data/ml-1m.train.rating"
negative_file_path = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/neural_collaborative_filtering/Data/ml-1m.test.negative"
path_to_save_figures = "/Users/florent/Documents/Eurecom/Projet NCF vs MF/Courbes/"

d = 32
h = int(d/2)
batch_size = 256
K = 10
epochs = 3

def number_users_and_items(path):
    file = open(file=path, mode='r')
    num_users, num_items = 0, 0
    for line in file:
        split_line = line.split('\t')[:-1]
        userID, itemID = int(split_line[0]), int(split_line[1])
        if userID > num_users:
            num_users = userID
        if itemID > num_items:
            num_items = itemID
    file.close()
    return num_users, num_items

def get_train_set(path, num_negative_instances):
    num_items = number_users_and_items(path)[1]
    data = []
    file = open(file=path, mode='r')
    current_user = 0
    interactions_per_user = []
    for line in file:
        split_line = line.split('\t')[:-1]
        user, item = int(split_line[0]), int(split_line[1])
        if user == current_user:
            interactions_per_user.append(item)
            data.append([user, item, 1])
        if user != current_user:
            num_interactions = len(interactions_per_user)
            for i in range(num_negative_instances * num_interactions):
                random_item = np.random.randint(num_items)
                while (random_item in interactions_per_user):
                    random_item = np.random.randint(num_items)
                data.append([current_user, random_item, 0])
            data.append([user, item, 1])
            current_user = user
            interactions_per_user = [item]
    num_interactions = len(interactions_per_user)
    for i in range(num_negative_instances * num_interactions):
        random_item = np.random.randint(num_items)
        while (random_item in interactions_per_user):
            random_item = np.random.randint(num_items)
        data.append([current_user, random_item, 0])
    return np.array(data)


num_users, num_items = number_users_and_items(path=train_file_path)

print("\n*** Preprocessing of data ***")
train_set = get_train_set(path=train_file_path, num_negative_instances=4)
print("Number of users : {}".format(num_users))
print("Number of items : {}".format(num_items))
print("Number of training interactions : {}".format(len(train_set)))
train_features = [train_set[:,0], train_set[:,1]]
train_labels = train_set[:,2]
print("Done !\n")

## MLP model
user_input_MLP = Input(shape=(1,), dtype='int32', name='user_input_MLP')
item_input_MLP = Input(shape=(1,), dtype='int32', name='item_input_MLP')
user_embedding_MLP = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_MLP')
item_embedding_MLP = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_MLP')
user_latent_MLP = Flatten()(user_embedding_MLP(user_input_MLP))
item_latent_MLP = Flatten()(item_embedding_MLP(item_input_MLP))
concatenation = Concatenate()([user_latent_MLP, item_latent_MLP])
layer1 = Dense(units=64, activation='relu', name='layer1')(concatenation)
layer2 = Dense(units=32, activation='relu', name='layer2')(layer1)
layer3 = Dense(units=16, activation='relu', name='layer3') (layer2)
layer4 = Dense(units=8, activation='relu', name='layer4') (layer3)
prediction_MLP = Dense(units=1, activation='sigmoid', name='prediction_MLP')(layer4)
MLP = Model(inputs=[user_input_MLP, item_input_MLP], outputs=prediction_MLP)
MLP.compile(optimizer=Adam(), loss=BinaryCrossentropy())
MLP.name = "MLP"


## GMF model
user_input_GMF = Input(shape=(1,), dtype='int32', name='user_input_GMF')
item_input_GMF = Input(shape=(1,), dtype='int32', name='item_input_GMF')
user_embedding_GMF = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_GMF')
item_embedding_GMF = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_GMF')
user_latent_GMF = Flatten()(user_embedding_GMF(user_input_GMF))
item_latent_GMF = Flatten()(item_embedding_GMF(item_input_GMF))
mul = Multiply()([user_latent_GMF, item_latent_GMF])
prediction_GMF = Dense(units=1, activation='sigmoid', name='prediction')(mul)
GMF = Model(inputs=[user_input_GMF, item_input_GMF], outputs=prediction_GMF)
GMF.compile(optimizer=Adam(), loss=BinaryCrossentropy())
GMF.name = "GMF"


## NeuMF model
user_input = Input(shape=(1,), dtype='int32', name='user_input_MLP')
item_input = Input(shape=(1,), dtype='int32', name='item_input_MLP')
## MLP part
user_embedding_MLP = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_MLP')
item_embedding_MLP = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_MLP')
user_latent_MLP = Flatten()(user_embedding_MLP(user_input))
item_latent_MLP = Flatten()(item_embedding_MLP(item_input))
concatenation_embeddings = Concatenate()([user_latent_MLP, item_latent_MLP])
layer1 = Dense(units=64, activation='relu', name='layer1')(concatenation_embeddings)
layer2 = Dense(units=32, activation='relu', name='layer2')(layer1)
layer3 = Dense(units=16, activation='relu', name='layer3') (layer2)
layer4 = Dense(units=8, activation='relu', name='layer4') (layer3)
## GMF part
user_embedding_GMF = Embedding(input_dim=num_users+1, output_dim=d, name='user_embedding_GMF')
item_embedding_GMF = Embedding(input_dim=num_items+1, output_dim=d, name='item_embedding_GMF')
user_latent_GMF = Flatten()(user_embedding_GMF(user_input))
item_latent_GMF = Flatten()(item_embedding_GMF(item_input))
mul = Multiply()([user_latent_GMF, item_latent_GMF])
concatenation_of_models = Concatenate()([mul, layer4])
prediction_NeuMF = Dense(units=1, activation='sigmoid', name='prediction')(concatenation_of_models)
NeuMF = Model(inputs=[user_input, item_input], outputs=prediction_NeuMF)
NeuMF.compile(optimizer=Adam(), loss=BinaryCrossentropy())
NeuMF.name = "NeuMF"


def getHitRatio(ranklist, K, positive_item):
    if positive_item in ranklist[:K]:
        return 1
    else:
        return 0

def getNDCG(ranklist, K, positive_item):
    if positive_item in ranklist[:K]:
        ranking_of_positive_item = np.where(ranklist == positive_item)[0][0]
        return math.log(2)/math.log(2+ranking_of_positive_item)
    else:
        return 0

def rank(item_scores):
    list_item_scores = item_scores.tolist()
    ranklist = sorted(list_item_scores, key=lambda item_score: item_score[1], reverse=True)
    ranklist = np.array(ranklist)[:,0].astype('int64')
    return ranklist

def evaluate_model(model, negative_file_path, K):
    hits, ndcgs = [],[]
    negative_file = open(file=negative_file_path, mode="r")
    for line in negative_file:
        split_line = line.split(sep="\t")
        (user, positive_item) = make_tuple(split_line[0])
        items = np.array([positive_item]+[int(split_line[i]) for i in range(1,len(split_line))])
        users = np.full(shape=len(items), fill_value=user)
        predictions = model.predict(x=[users,items], batch_size=100, verbose=0)
        map_item_scores = np.concatenate((items.reshape((100,1)), predictions), axis=1)
        ranklist_items = rank(item_scores=map_item_scores)
        hr = getHitRatio(ranklist=ranklist_items, K=K, positive_item=positive_item)
        ndcg = getNDCG(ranklist=ranklist_items, K=K, positive_item=positive_item)
        hits.append(hr)
        ndcgs.append(ndcg)
    hits = np.array(hits)
    ndcgs = np.array(ndcgs)
    return hits.mean(), ndcgs.mean()

def evaluate_models(models, negative_file_path, K):
    hits, ndcgs = [], []
    negative_file = open(file=negative_file_path, mode="r")
    for line in negative_file:
        split_line = line.split(sep="\t")
        (user, positive_item) = make_tuple(split_line[0])
        items = np.array([positive_item]+[int(split_line[i]) for i in range(1,len(split_line))])
        users = np.full(shape=len(items), fill_value=user)
        hits_models, ndcgs_models = [], []
        for model in models:
            predictions = model.predict(x=[users, items], batch_size=100, verbose=0)
            map_item_scores = np.concatenate((items.reshape((100, 1)), predictions), axis=1)
            ranklist_items = rank(item_scores=map_item_scores)
            hr = getHitRatio(ranklist=ranklist_items, K=K, positive_item=positive_item)
            ndcg = getNDCG(ranklist=ranklist_items, K=K, positive_item=positive_item)
            hits_models.append(hr)
            ndcgs_models.append(ndcg)
        hits.append(hits_models)
        ndcgs.append(ndcgs_models)
    hits, ndcgs = np.array(hits), np.array(ndcgs)
    #hr_per_models = np.array([hits[:,i].mean() for i in range(len(models))])
    #ndcg_per_models = np.array([ndcgs[:,i].mean() for i in range(len(models))])
    hr_per_models = hits.mean(axis=0)
    ndcg_per_models = ndcgs.mean(axis=0)
    return hr_per_models, ndcg_per_models

#MLP.summary()
#GMF.summary()
#NeuMF.summary()

print("\n*** Training of models ***\n")

models  =[MLP, GMF, NeuMF]

#first_hit_MLP, first_ndcg_MLP = evaluate_model(model=MLP, negative_file_path=negative_file_path, K=K)
#first_hit_GMF, first_ndcg_GMF = evaluate_model(model=GMF, negative_file_path=negative_file_path, K=K)
first_hits, first_ndcgs = evaluate_models(models=models, negative_file_path=negative_file_path, K=K)
first_hit_MLP, first_hit_GMF, first_hit_NeuMF = first_hits[0], first_hits[1], first_hits[2]
first_ndcg_MLP, first_ndcg_GMF, first_ndcg_NeuMF = first_ndcgs[0], first_ndcgs[1], first_ndcgs[2]

first_loss_MLP = MLP.evaluate(x=train_features, y=train_labels, batch_size=batch_size, verbose=0)
first_loss_GMF = GMF.evaluate(x=train_features, y=train_labels, batch_size=batch_size, verbose=0)
first_loss_NeuMF = NeuMF.evaluate(x=train_features, y=train_labels, batch_size=batch_size, verbose=0)

hits_MLP, ndcgs_MLP, loss_MLP = [first_hit_MLP], [first_ndcg_MLP], [first_loss_MLP]
hits_GMF, ndcgs_GMF, loss_GMF = [first_hit_GMF], [first_ndcg_GMF], [first_loss_GMF]
hits_NeuMF, ndcgs_NeuMF, loss_NeuMF = [first_hit_NeuMF], [first_ndcg_NeuMF], [first_loss_NeuMF]

for e in range(epochs):
    print("\nEpoch n°{}/{}".format(e+1, epochs))
    history_MLP = MLP.fit(x=train_features, y=train_labels, batch_size=batch_size, epochs=1, verbose=2, shuffle=True)
    history_GMF = GMF.fit(x=train_features, y=train_labels, batch_size=batch_size, epochs=1, verbose=2, shuffle=True)
    history_NeuMF = NeuMF.fit(x=train_features, y=train_labels, batch_size=batch_size, epochs=1, verbose=2, shuffle=True)
    #hit_MLP, ndcg_MLP = evaluate_model(model=MLP, negative_file_path=negative_file_path, K=K)
    #hit_GMF, ndcg_GMF = evaluate_model(model=GMF, negative_file_path=negative_file_path, K=K)
    hits, ndcgs = evaluate_models(models=models, negative_file_path=negative_file_path, K=K)
    #hits_MLP.append(hit_MLP)
    hits_MLP.append(hits[0])
    #ndcgs_MLP.append(ndcg_MLP)
    ndcgs_MLP.append(ndcgs[0])
    #hits_GMF.append(hit_GMF)
    hits_GMF.append(hits[1])
    #ndcgs_GMF.append(ndcg_GMF)
    ndcgs_GMF.append(ndcgs[1])
    hits_NeuMF.append(hits[2])
    ndcgs_NeuMF.append(ndcgs[2])
    loss_MLP.append(history_MLP.history["loss"][0])
    loss_GMF.append(history_GMF.history["loss"][0])
    loss_NeuMF.append(history_NeuMF.history["loss"][0])

run_time = time()-start
print("Running time : {:.0f} min {:.0f} sec".format(run_time//60, run_time%60))

iterations = [e for e in range(epochs+1)]
color_MLP, color_GMF, color_NeuMF = "rebeccapurple", "mediumblue", "red"

plt.figure("Loss")
plt.plot(iterations, loss_MLP, color=color_MLP)
plt.plot(iterations, loss_GMF, color=color_GMF, linestyle="dashed")
plt.plot(iterations, loss_NeuMF, color=color_NeuMF)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("Training loss")
plt.legend(("MLP","GMF","NeuMF",))
plt.title("Loss")
plt.grid()
plt.savefig(fname=path_to_save_figures+"loss.png")

plt.figure("Hit Ratio")
plt.plot(iterations, hits_MLP, color=color_MLP)
plt.plot(iterations, hits_GMF, color=color_GMF, linestyle="dashed")
plt.plot(iterations, hits_NeuMF, color=color_NeuMF)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("Hit Ratio")
plt.legend(("MLP","GMF","NeuMF",))
plt.title("Hit Ratio")
plt.grid()
plt.savefig(fname=path_to_save_figures+"hr.png")

plt.figure("NDCG")
plt.plot(iterations, ndcgs_MLP, color=color_MLP)
plt.plot(iterations, ndcgs_GMF, color=color_GMF, linestyle="dashed")
plt.plot(iterations, ndcgs_NeuMF, color=color_NeuMF)
plt.xticks(ticks=iterations)
plt.xlabel("Iteration")
plt.ylabel("NDCG")
plt.legend(("MLP","GMF","NeuMF",))
plt.title("NDCG")
plt.grid()
plt.savefig(fname=path_to_save_figures+"ndcg.png")

plt.show()
